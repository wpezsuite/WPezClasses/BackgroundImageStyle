## WPezClasses: BackgroundImageStyle


__The ezWay to generate the \<style>...background-image: url()...\</style> with breakpoints for a given WordPress image__

Do themes like Twenty Seventeen bother you? That is, the hero image is fixed at 2000x1200 for all screen sizes, even XS ones? If you're better than that then this class is for you.      

> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --

### Simple Example


``` 
use WPez\WPezClasses\BackgroundImageStyle\ClassBackgroundImageStyle as BIS;

if ( have_posts() ) :

    $new_bis = new BIS();

    while ( have_posts() ) : the_post();

        $int_gpt_id = get_post_thumbnail_id( get_the_ID());
        if ( ! empty( $int_gpt_id)) {
        
            // set the image ID
            $new_bis->setAttachmentID($int_gpt_id);
            // selector the CSS is generated for? 
            $new_bis->setSelector( 'post-' . get_the_ID() );
            // what WP image size should be used for the XS breakpoint?
            $new_bis->pushXS( 'medium_large' );
            // what WP image size should be used for the MD breakpoint?
            $new_bis->pushMD( 'novella-archive-cover' );
            // what WP image size should be used for the XL breakpoint?
            $new_bis->pushXL( 'novella-archive-cover-max2k' );
            
        }

        // this template part has a div with id="post-*" for each image
        get_template_part( 'content/archive-post' );

        $new_bis->setReset();

    endwhile;

    // outout the style
    $new_bis->getStyle( true );
    // FYI - you can also do:
    // echo $new_bis->getStyle(false);

else :

    get_template_part( 'content', 'none' );

endif;


```

The getStyle() will echo something like this:

```
<style>#post-3: url(https:/***.com/wp-content/uploads/2018/11/some-img-file-name-768x512.jpg);}  @media screen and (min-width:768px){#novella-entry-mast-img{background-image: url(https:/***.com/wp-content/uploads/2018/11/some-img-file-name-1200x1365.jpg);}}   @media screen and (min-width:1200px){#novella-entry-mast-img{background-image: url(https:/***.com/wp-content/uploads/2018/11/some-img-file-name-2048x1365.jpg);}}  </style>
```

_**Important**_ 

1) It will be up to you to code and further style \<div id=#post-3 class="my-background-image-styles">...\</div>. For example, if you want to use background-size: cover, you'd have to put that in your own CSS somewhere. The purpose of this class is to make it ez to generate the \<style>...background-image: url()...\</style>.

2) You can have the code above within a loop, but with the getStyle() outside the loop, in order to generate a single \<style>...\</style> (as opposed to outputting them individually for each image).


### FAQ

**1) Can I use this in my plugin or theme?**

Yes, but to be safe, please change the namespace. 

### HELPFUL LINKS

- Here's a working example: https://gitlab.com/WPezPlugins/wpez-wc-storefront-parallax-hero-plus/blob/master/App/Plugin/Plus/ClassPlus.php

### TODO

- Provide a better example(s)
- Explore the need for a broader more complete reset



### CHANGE LOG

- v0.5.2 - 22 March 2019

    - ADDED bool flag to the set{BP} methods. true adds !important to the property / value generated.
    - ADDED method: loadSizes() - for adding size => wp_img_size in one load, instead of the individual setXS(), setSM(), etc. 

- v0.5.1 - 3 March 2019

    - ADDED method: resetPushArray() - See comment in code

- v0.5.0 - 14 Feb 2019
   
   Finally getting around to repo'ing this. 